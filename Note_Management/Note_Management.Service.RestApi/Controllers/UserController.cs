﻿using Microsoft.AspNetCore.Mvc;
using Note_Management.Service.RestApi.Models.Dtos;
using Note_Management.Service.RestApi.Repositories.UserRepository;

namespace Note_Management.Service.RestApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<UserDto> GetUserAsync(string userId)
        {
            return await _userRepository.GetUserAsync(userId);
        }

        [HttpGet]
        public async Task<IEnumerable<UserDto>> GetUsersAsync()
        {
            return await _userRepository.GetUsersAsync();
        }

        [HttpPost]
        public async Task<UserDto> AddUserAsync([FromBody] UserDto user)
        {
            return await _userRepository.AddUserAsync(user);
        }

        [HttpPut]
        public async Task<UserDto> UpdateUserAsync([FromBody] UserDto user)
        {
            return await _userRepository.UpdateUserAsync(user);
        }

        [HttpDelete]
        public async Task<bool> DeleteUserAsync(string userId)
        {
            return await _userRepository.DeleteUserAsync(userId);
        }

    }
}
