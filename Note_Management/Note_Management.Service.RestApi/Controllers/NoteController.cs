﻿using Microsoft.AspNetCore.Mvc;
using Note_Management.Service.RestApi.Models.Dtos;
using Note_Management.Service.RestApi.Repositories.NoteRepository;


namespace Note_Management.Service.RestApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class NoteController : Controller
    {
        private readonly INoteRepository _noteRepository;

        public NoteController(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        [HttpGet]
        public async Task<NoteDto> GetNoteAsync(int noteId)
        {
            return await _noteRepository.GetNoteAsync(noteId);
        }

        [HttpGet]
        public async Task<IEnumerable<NoteDto>> GetNotesAsync(string userId)
        {
            return await _noteRepository.GetNotesAsync(userId);
        }

        [HttpPost]
        public async Task<NoteDto> AddNoteAsync([FromBody] NoteDto note, string userId)
        {
            return await _noteRepository.AddNoteAsync(note, userId);
        }

        [HttpPut]
        public async Task<NoteDto> UpdateNoteAsync([FromBody] NoteDto note, string userId)
        {
            return await _noteRepository.UpdateNoteAsync(note, userId);
        }

        [HttpDelete]
        public async Task<bool> DeleteNoteAsync(int noteId, string userId)
        {
            return await _noteRepository.DeleteNoteAsync(noteId, userId);
        }

    }
}
