﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Note_Management.Service.RestApi.Models.Dtos;
using Note_Management.Service.RestApi.Models;

namespace Note_Management.Service.RestApi.Configurations
{
    public class MappingConfiguration : Profile
    {
        public MappingConfiguration()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();

            CreateMap<Note, NoteDto>();
            CreateMap<NoteDto, Note>();

            CreateMap<User, IdentityUser>();
        }
    }
}
