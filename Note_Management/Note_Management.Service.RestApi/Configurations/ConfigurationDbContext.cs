﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Note_Management.Service.RestApi.Models;

namespace Note_Management.Service.RestApi.Configurations
{
    public class ConfigurationDbContext : IdentityDbContext
    {
        public DbSet<Note> Notes { get; set; }
        private DbSet<User> Users { get; set; }

        public ConfigurationDbContext(DbContextOptions<ConfigurationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
