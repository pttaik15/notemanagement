﻿using Note_Management.Service.RestApi.Configurations;
using Note_Management.Service.RestApi.Repositories.NoteRepository;
using Note_Management.Service.RestApi.Repositories.UserRepository;

namespace Note_Management.Service.RestApi.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ConfigurationDbContext _context;

        public IUserRepository UserRepository { get; set; }
        public INoteRepository NoteRepository { get; set; }

        public UnitOfWork(ConfigurationDbContext context, IUserRepository userRepository, INoteRepository noteRepository)
        {
            _context = context;
            UserRepository = userRepository;
            NoteRepository = noteRepository;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
