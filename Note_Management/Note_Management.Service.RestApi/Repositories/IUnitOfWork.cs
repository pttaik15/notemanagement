﻿using Note_Management.Service.RestApi.Repositories.NoteRepository;
using Note_Management.Service.RestApi.Repositories.UserRepository;

namespace Note_Management.Service.RestApi.Repositories
{
    public interface IUnitOfWork
    {
        IUserRepository UserRepository { get; }
        INoteRepository NoteRepository { get; }

        Task<int> SaveChangesAsync();
    }
}
