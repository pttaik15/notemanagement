﻿using Note_Management.Service.RestApi.Models.Dtos;

namespace Note_Management.Service.RestApi.Repositories.NoteRepository
{
    public interface INoteRepository
    {
        Task<NoteDto> GetNoteAsync(int noteId);
        Task<IEnumerable<NoteDto>> GetNotesAsync(string userId);
        Task<NoteDto> AddNoteAsync(NoteDto note, string userId);
        Task<NoteDto> UpdateNoteAsync(NoteDto note, string userId);
        Task<bool> DeleteNoteAsync(int noteId, string userId);
    }
}
