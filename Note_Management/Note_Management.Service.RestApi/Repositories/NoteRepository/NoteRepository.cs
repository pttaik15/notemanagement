﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Note_Management.Service.RestApi.Configurations;
using Note_Management.Service.RestApi.Models;
using Note_Management.Service.RestApi.Models.Dtos;
using Note_Management.Service.RestApi.Repositories.UserRepository;

namespace Note_Management.Service.RestApi.Repositories.NoteRepository
{
    public class NoteRepository : INoteRepository
    {
        private readonly ConfigurationDbContext _context;
        private readonly IMapper _mapper;

        public NoteRepository(ConfigurationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<NoteDto> AddNoteAsync(NoteDto note, string userId)
        {
            var user = await _context.Users.FindAsync(userId);
            if (user == null)
            {
                throw new ArgumentException($"User with id '{userId}' not found.");
            }

            var noteModel = _mapper.Map<Note>(note);
            noteModel.UserId = userId;

            _context.Notes.Add(noteModel);
            await _context.SaveChangesAsync();

            return _mapper.Map<NoteDto>(noteModel);
        }

        public async Task<bool> DeleteNoteAsync(int noteId, string userId)
        {
            var note = await _context.Notes.FindAsync(noteId);
            if (note == null)
            {
                return false;
            }

            if (note.UserId != userId)
            {
                throw new ArgumentException($"Note with id '{noteId}' does not belong to the user with id '{userId}'.");
            }

            _context.Notes.Remove(note);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<NoteDto> GetNoteAsync(int noteId)
        {
            var note = await _context.Notes.FindAsync(noteId);
            if (note == null)
            {
                return null;
            }

            return _mapper.Map<NoteDto>(note);
        }

        public async Task<IEnumerable<NoteDto>> GetNotesAsync(string userId)
        {
            var notes = await _context.Notes.Where(n => n.UserId == userId).ToListAsync();
            return _mapper.Map<IEnumerable<NoteDto>>(notes);
        }

        public async Task<NoteDto> UpdateNoteAsync(NoteDto note, string userId)
        {
            var noteModel = _mapper.Map<Note>(note);
            if (noteModel.UserId != userId)
            {
                throw new ArgumentException($"Note with id '{note.NoteId}' does not belong to the user with id '{userId}'.");
            }

            _context.Notes.Update(noteModel);
            await _context.SaveChangesAsync();

            return _mapper.Map<NoteDto>(noteModel);
        }
    }
}
