﻿using Note_Management.Service.RestApi.Configurations;
using Note_Management.Service.RestApi.Models.Dtos;
using Note_Management.Service.RestApi.Models;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Identity;

namespace Note_Management.Service.RestApi.Repositories.UserRepository
{
    public class UserRepository : IUserRepository
    {
        private readonly ConfigurationDbContext _context;
        private readonly IMapper _mapper;

        public UserRepository(ConfigurationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<UserDto> AddUserAsync(UserDto user)
        {
            var userModel = _mapper.Map<User>(user);
            var identityUser = _mapper.Map<IdentityUser>(userModel);
            await _context.Users.AddAsync(identityUser);
            await _context.SaveChangesAsync();
            return _mapper.Map<UserDto>(userModel);
        }

        public async Task<bool> DeleteUserAsync(string userId)
        {
            var user = await _context.Users.FindAsync(userId);
            if (user == null)
            {
                return false;
            }
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<UserDto> GetUserAsync(string userId)
        {
            var user = await _context.Users.FindAsync(userId);
            return _mapper.Map<UserDto>(user);
        }

        public async Task<IEnumerable<UserDto>> GetUsersAsync()
        {
            var users = await _context.Users.ToListAsync();
            return _mapper.Map<IEnumerable<UserDto>>(users);
        }

        public async Task<UserDto> UpdateUserAsync(UserDto user)
        {
            var userModel = _mapper.Map<User>(user);
            var identityUser = _mapper.Map<IdentityUser>(userModel);
            _context.Users.Update(identityUser);
            await _context.SaveChangesAsync();
            return _mapper.Map<UserDto>(userModel);
        }
    }
}
