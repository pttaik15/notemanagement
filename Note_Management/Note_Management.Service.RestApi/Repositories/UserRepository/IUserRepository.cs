﻿using Note_Management.Service.RestApi.Models.Dtos;

namespace Note_Management.Service.RestApi.Repositories.UserRepository
{
    public interface IUserRepository
    {
        Task<UserDto> GetUserAsync(string userId);
        Task<IEnumerable<UserDto>> GetUsersAsync();
        Task<UserDto> AddUserAsync(UserDto user);
        Task<UserDto> UpdateUserAsync(UserDto user);
        Task<bool> DeleteUserAsync(string userId);
    }
}
