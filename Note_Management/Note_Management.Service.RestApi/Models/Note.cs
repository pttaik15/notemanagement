﻿using System.ComponentModel.DataAnnotations;

namespace Note_Management.Service.RestApi.Models
{
    public class Note
    {
        [Key]
        public int NoteId { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string? Title { get; set; }

        [Required]
        [StringLength(1000, MinimumLength = 10)]
        public string? Content { get; set; }

        [Required]
        public DateTime? CreatedAt { get; set; }

        [Required]
        public string? UserId { get; set; }

        public User? User { get; set; }
    }
}
