﻿using System.ComponentModel.DataAnnotations;

namespace Note_Management.Service.RestApi.Models
{
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        [Key]
        public string? UserId { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string? Name { get; set; }

        [Required]
        [EmailAddress]
        public string? Email { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 6)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,100}$",
            ErrorMessage = "Password must have at least one uppercase letter, one lowercase letter, one digit, and one special character")]
        public string? Password { get; set; }


        [Required]
        public string? Role { get; set; }

        [Required]
        [Phone]
        public decimal? Phone { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 10)]
        public string? Address { get; set; }
    }

}
