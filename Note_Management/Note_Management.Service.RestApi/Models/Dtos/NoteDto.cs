﻿namespace Note_Management.Service.RestApi.Models.Dtos
{
    public class NoteDto
    {
        public int NoteId { get; set; }
        public string? Title { get; set; }
        public string? Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? UserId { get; set; }
    }
}
